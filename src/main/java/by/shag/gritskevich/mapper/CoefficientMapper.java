package by.shag.gritskevich.mapper;

import java.util.List;

import by.shag.gritskevich.api.dto.CoefficientDto;
import by.shag.gritskevich.jpa.model.Coefficient;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface CoefficientMapper {

    @Mapping(target = "id", ignore = true)
    Coefficient mapFromDto(CoefficientDto coefficientDto);


    CoefficientDto mapFromModel(Coefficient coefficient);


    List<Coefficient> mapListFromDto(List<CoefficientDto> coefficientDtoList);


    List<CoefficientDto> mapListFromModel(List<Coefficient> coefficientList);

}
