package by.shag.gritskevich.mapper;


import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import by.shag.gritskevich.api.dto.PeriodOfDayPromoDto;
import by.shag.gritskevich.jpa.model.PeriodOfDayPromo;


@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface PeriodOfDayPromoMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "pricing", ignore = true)
    PeriodOfDayPromo mapFromDto(PeriodOfDayPromoDto periodOfDayPromoDto);


    @Mapping(target = "pricingId", source = "pricing.id")
    PeriodOfDayPromoDto mapFromModel(PeriodOfDayPromo periodOfDayPromo);


    List<PeriodOfDayPromo> mapListFromDto(List<PeriodOfDayPromoDto> PeriodOfDayPromoList);


    List<PeriodOfDayPromoDto> mapListFromModel(List<PeriodOfDayPromo> PeriodOfDayPromoList);
}
