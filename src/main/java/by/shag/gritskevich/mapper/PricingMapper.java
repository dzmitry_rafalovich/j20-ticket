package by.shag.gritskevich.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import by.shag.gritskevich.api.dto.PricingDto;
import by.shag.gritskevich.jpa.model.Pricing;


@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface PricingMapper {

    @Mapping(target = "id", ignore = true)
    Pricing mapFromDto(PricingDto pricingDto);


    PricingDto mapFromModel(Pricing pricing);


    List<Pricing> mapListFromDto(List<PricingDto> pricingDtoList);


    List<PricingDto> mapListFromModel(List<Pricing> pricingList);
}
