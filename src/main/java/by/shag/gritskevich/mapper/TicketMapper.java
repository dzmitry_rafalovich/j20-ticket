package by.shag.gritskevich.mapper;

import java.util.List;

import by.shag.gritskevich.api.dto.TicketDto;
import by.shag.gritskevich.jpa.model.Ticket;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface TicketMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "pricing", ignore = true)
    @Mapping(target = "coefficient", ignore = true)
    Ticket mapFromDto(TicketDto ticketDto);

    @Mapping(target = "pricingId", source = "pricing.id")
    @Mapping(target = "coefficientId", source = "coefficient.id")
    TicketDto mapFromModel(Ticket ticket);


    List<Ticket> mapListFromDto(List<TicketDto> ticketDtoList);


    List<TicketDto> mapListFromModel(List<Ticket> ticketList);
}
