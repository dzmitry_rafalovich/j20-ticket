package by.shag.gritskevich.service;


import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import by.shag.gritskevich.api.dto.PeriodOfDayPromoDto;
import by.shag.gritskevich.exceptions.EntityNotFoundException;
import by.shag.gritskevich.jpa.repository.PeriodOfDayPromoRepository;
import by.shag.gritskevich.mapper.PeriodOfDayPromoMapper;


@RequiredArgsConstructor
@Service
public class PeriodOfDayPromoService {

    private final PeriodOfDayPromoRepository periodOfDayPromoRepository;

    private final PeriodOfDayPromoMapper periodOfDayPromoMapper;


    public PeriodOfDayPromoDto save(PeriodOfDayPromoDto periodOfDayPromoDto) {
        var periodOfDayPromo = periodOfDayPromoMapper.mapFromDto(periodOfDayPromoDto);
        periodOfDayPromo = periodOfDayPromoRepository.save(periodOfDayPromo);
        return periodOfDayPromoMapper.mapFromModel(periodOfDayPromo);
    }


    public PeriodOfDayPromoDto findById(Long id) {
        var optionalPeriodOfDayPromo = periodOfDayPromoRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("PeriodOfDayPromo with ID = " + id + " not found"));
        return periodOfDayPromoMapper.mapFromModel(optionalPeriodOfDayPromo);
    }


    public List<PeriodOfDayPromoDto> findAll() {
        var periodOfDayPromoList = periodOfDayPromoRepository.findAll();
        return periodOfDayPromoMapper.mapListFromModel(periodOfDayPromoList);
    }


    public PeriodOfDayPromoDto update(Long id, PeriodOfDayPromoDto periodOfDayPromoDto) {
        var periodOfDayPromo = periodOfDayPromoMapper.mapFromDto(periodOfDayPromoDto);
        periodOfDayPromo.setId(id);
        periodOfDayPromo = periodOfDayPromoRepository.save(periodOfDayPromo);
        return periodOfDayPromoMapper.mapFromModel(periodOfDayPromo);
    }


    public void deleteById(Long id) {
        periodOfDayPromoRepository.deleteById(id);
    }
}
