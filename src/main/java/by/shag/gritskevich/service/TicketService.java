package by.shag.gritskevich.service;


import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import by.shag.gritskevich.api.dto.TicketDto;
import by.shag.gritskevich.exceptions.EntityNotFoundException;
import by.shag.gritskevich.jpa.repository.TicketRepository;
import by.shag.gritskevich.mapper.TicketMapper;


@RequiredArgsConstructor
@Service
public class TicketService {

    private final TicketRepository ticketRepository;

    private final TicketMapper ticketMapper;


    public TicketDto save(TicketDto ticketDto) {
        var ticket = ticketMapper.mapFromDto(ticketDto);
        ticket = ticketRepository.save(ticket);
        return ticketMapper.mapFromModel(ticket);
    }


    public TicketDto findById(Long id) {
        var optionalTicket = ticketRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Ticket with ID = " + id + " not found"));
        return ticketMapper.mapFromModel(optionalTicket);
    }


    public List<TicketDto> findAll() {
        var ticketList = ticketRepository.findAll();
        return ticketMapper.mapListFromModel(ticketList);
    }


    public TicketDto update(Long id, TicketDto ticketDto) {
        var ticket = ticketMapper.mapFromDto(ticketDto);
        ticket.setId(id);
        ticket = ticketRepository.save(ticket);
        return ticketMapper.mapFromModel(ticket);
    }


    public void deleteById(Long id) {
        ticketRepository.deleteById(id);
    }
}
