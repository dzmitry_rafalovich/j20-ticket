package by.shag.gritskevich.service;


import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import by.shag.gritskevich.api.dto.CoefficientDto;
import by.shag.gritskevich.mapper.CoefficientMapper;
import by.shag.gritskevich.exceptions.EntityNotFoundException;
import by.shag.gritskevich.jpa.repository.CoefficientRepository;


@RequiredArgsConstructor
@Service
public class CoefficientService {

    private final CoefficientRepository coefficientRepository;

    private final CoefficientMapper coefficientMapper;


    public CoefficientDto save(CoefficientDto coefficientDto) {
        var coefficient = coefficientMapper.mapFromDto(coefficientDto);
        coefficient = coefficientRepository.save(coefficient);
        return coefficientMapper.mapFromModel(coefficient);
    }


    public CoefficientDto findById(Long id) {
        var optionalCoefficient = coefficientRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Coefficient with ID = " + id + " not found"));
        return coefficientMapper.mapFromModel(optionalCoefficient);
    }


    public List<CoefficientDto> findAll() {
        var coefficientList = coefficientRepository.findAll();
        return coefficientMapper.mapListFromModel(coefficientList);
    }


    public CoefficientDto update(Long id, CoefficientDto coefficientDto) {
        var coefficient = coefficientMapper.mapFromDto(coefficientDto);
        coefficient.setId(id);
        coefficient = coefficientRepository.save(coefficient);
        return coefficientMapper.mapFromModel(coefficient);
    }


    public void deleteById(Long id) {
        coefficientRepository.deleteById(id);
    }
}
