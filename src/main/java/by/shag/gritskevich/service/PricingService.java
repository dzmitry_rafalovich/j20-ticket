package by.shag.gritskevich.service;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import by.shag.gritskevich.api.dto.PricingDto;
import by.shag.gritskevich.exceptions.EntityNotFoundException;
import by.shag.gritskevich.jpa.repository.PricingRepository;
import by.shag.gritskevich.mapper.PricingMapper;


@RequiredArgsConstructor
@Service
public class PricingService {

    private final PricingRepository pricingRepository;

    private final PricingMapper pricingMapper;


    public PricingDto save(PricingDto pricingDto) {
        var pricing = pricingMapper.mapFromDto(pricingDto);
        pricing = pricingRepository.save(pricing);
        return pricingMapper.mapFromModel(pricing);
    }


    public PricingDto findById(Long id) {
        var optionalPricing = pricingRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Pricing with ID = " + id + " not found"));
        return pricingMapper.mapFromModel(optionalPricing);
    }


    public List<PricingDto> findAll() {
        var pricingList = pricingRepository.findAll();
        return pricingMapper.mapListFromModel(pricingList);
    }


    public PricingDto update(Long id, PricingDto pricingDto) {
        var pricing = pricingMapper.mapFromDto(pricingDto);
        pricing.setId(id);
        pricing = pricingRepository.save(pricing);
        return pricingMapper.mapFromModel(pricing);
    }


    public void deleteById(Long id) {
        pricingRepository.deleteById(id);
    }
}
