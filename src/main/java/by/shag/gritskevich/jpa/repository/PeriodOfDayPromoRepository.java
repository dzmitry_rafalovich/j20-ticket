package by.shag.gritskevich.jpa.repository;

import by.shag.gritskevich.jpa.model.PeriodOfDayPromo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PeriodOfDayPromoRepository extends JpaRepository<PeriodOfDayPromo, Long> {

}
