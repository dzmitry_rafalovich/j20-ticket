package by.shag.gritskevich.jpa.repository;

import by.shag.gritskevich.jpa.model.Pricing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PricingRepository extends JpaRepository<Pricing, Long> {

}
