package by.shag.gritskevich.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Table(name = "pricing")
@Entity
public class Pricing {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "film_id", nullable = false)
    private Long filmId;

    @Column(name = "cinema_id", nullable = false)
    private Long cinemaId;

    @Column(name = "hall_id", nullable = false)
    private Long hallId;

    @Column(name = "default_price", nullable = false)
    private Integer defaultPrice;

}
