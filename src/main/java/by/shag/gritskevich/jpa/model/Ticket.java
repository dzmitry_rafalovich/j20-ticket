package by.shag.gritskevich.jpa.model;

import java.time.Instant;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Data
@Table(name = "ticket")
@Entity
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "session_id", nullable = false)
    private Long sessionId;

    @ManyToOne
    @JoinColumn(name = "pricing_id", nullable = false)
    private Pricing pricing;

    @ManyToOne
    @JoinColumn(name = "coefficient_id", nullable = false)
    private Coefficient coefficient;

    @Column(name = "chair_id", nullable = false)
    private Long chairId;

    @Column(name = "created_at", nullable = false)
    private Instant createdAt;

    @Column(name = "final_price", nullable = false)
    private Integer finalPrice;

    @Column(name = "price_code", nullable = false)
    private Long priceCode;

}
