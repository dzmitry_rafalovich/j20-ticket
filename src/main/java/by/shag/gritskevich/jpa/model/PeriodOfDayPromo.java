package by.shag.gritskevich.jpa.model;

import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "period_of_day_promo")
@Entity
public class PeriodOfDayPromo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "pricing_id", nullable = false)
    private Pricing pricing;

    @Column(name = "start_hour", nullable = false)
    private Instant startHour;

    @Column(name = "end_hour", nullable = false)
    private Instant endHour;

    @Column(nullable = false)
    private Integer sail;

}
