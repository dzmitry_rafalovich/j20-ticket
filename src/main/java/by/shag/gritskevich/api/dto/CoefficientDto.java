package by.shag.gritskevich.api.dto;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import by.shag.gritskevich.jpa.model.ChairType;


@Data
public class CoefficientDto {

    @ApiModelProperty(value = "id", example = "1", position = 1)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @ApiModelProperty(value = "Chair type (DEFAULT)", example = "DEFAULT", required = true, position = 2)
    @NotNull(message = "Chair type should be not null")
    private ChairType chairType;

    @ApiModelProperty(value = "Price in cents", example = "5", required = true, position = 3)
    @NotNull(message = "Coefficient should be not null")
    @Min(value = 1, message = "Coefficient should be more than 0")
    private Integer coefficient;
}
