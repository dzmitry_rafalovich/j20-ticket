package by.shag.gritskevich.api.dto;


import java.time.Instant;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class TicketDto {

    @ApiModelProperty(value = "id", example = "1", position = 1)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @ApiModelProperty(value = "Session id", example = "1", required = true, position = 2)
    @NotNull(message = "Session id should be not null")
    @Min(value = 1, message = "Session id should be more than 0")
    private Long sessionId;

    @ApiModelProperty(value = "Pricing id", example = "1", required = true, position = 3)
    @NotNull(message = "Pricing id should be not null")
    @Min(value = 1, message = "Pricing id should be more than 0")
    private Long pricingId;

    @ApiModelProperty(value = "Coefficient id", example = "1", required = true, position = 4)
    @NotNull(message = "Coefficient id should be not null")
    @Min(value = 1, message = "Coefficient id should be more than 0")
    private Long coefficientId;

    @ApiModelProperty(value = "Chair id", example = "1", required = true, position = 5)
    @NotNull(message = "Chair id should be not null")
    @Min(value = 1, message = "Chair id should be more than 0")
    private Long chairId;

    @ApiModelProperty(value = "Created at", example = "2021-09-09T00:00:00.0Z", required = true, position = 6)
    @NotNull(message = "Created at should be not null")
    @Past(message = "Created at cannot be later than current date and time")
    private Instant createdAt;

    @ApiModelProperty(value = "Final price", example = "1", required = true, position = 7)
    @NotNull(message = "Final price should be not null")
    @Min(value = 1, message = "Final price should be more than 0")
    private Integer finalPrice;

    @ApiModelProperty(value = "Price code", example = "1", required = true, position = 8)
    @NotNull(message = "Price code should be not null")
    @Min(value = 1, message = "Price code should be more than 0")
    private Long priceCode;
}
