package by.shag.gritskevich.api.dto;


import java.time.Instant;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class PeriodOfDayPromoDto {

    @ApiModelProperty(value = "id", example = "1", position = 1)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @ApiModelProperty(value = "Pricing id", example = "5", required = true, position = 2)
    @NotNull(message = "Pricing id should be not null")
    @Min(value = 1, message = "Pricing id should be more than 0")
    private Long pricingId;

    @ApiModelProperty(value = "Start hour", example = "2021-08-31T00:00:00.0Z", required = true, position = 3)
    @NotNull(message = "Start hour must be not null")
    private Instant startHour;

    @ApiModelProperty(value = "End hour", example = "2021-08-31T00:00:00.0Z", required = true, position = 4)
    @NotNull(message = "End hour must be not null")
    private Instant endHour;

    @ApiModelProperty(value = "Sail id", example = "50", required = true, position = 5)
    @NotNull(message = "Sail id should be not null")
    @Min(value = 1, message = "Sail id should be more than 0")
    private Integer sail;
}
