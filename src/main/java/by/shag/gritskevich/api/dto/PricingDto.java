package by.shag.gritskevich.api.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class PricingDto {

    @ApiModelProperty(value = "id", example = "1", position = 1)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @ApiModelProperty(value = "Film id", example = "5", required = true, position = 2)
    @NotNull(message = "Film id should be not null")
    @Min(value = 1, message = "Film id should be more than 0")
    private Long filmId;

    @ApiModelProperty(value = "Cinema id", example = "5", required = true, position = 3)
    @NotNull(message = "Cinema id should be not null")
    @Min(value = 1, message = "Cinema id should be more than 0")
    private Long cinemaId;

    @ApiModelProperty(value = "Hall id", example = "5", required = true, position = 4)
    @NotNull(message = "Hall id should be not null")
    @Min(value = 1, message = "Hall id should be more than 0")
    private Long hallId;

    @ApiModelProperty(value = "Default price", example = "50", required = true, position = 5)
    @NotNull(message = "Default price should be not null")
    @Min(value = 1, message = "Default price should be more than 0")
    private Integer defaultPrice;

}
