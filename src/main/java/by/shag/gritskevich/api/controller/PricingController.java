package by.shag.gritskevich.api.controller;


import java.util.List;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

import by.shag.gritskevich.api.dto.PricingDto;
import by.shag.gritskevich.service.PricingService;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/pricing")
@RequiredArgsConstructor
public class PricingController {

    private final PricingService pricingService;


    @ApiOperation(value = "Save")
    @PostMapping("/save")
    public PricingDto save(@RequestBody PricingDto dto) {
        return pricingService.save(dto);
    }


    @ApiOperation(value = "Find by Id")
    @GetMapping("/{id}")
    public PricingDto findById(@PathVariable("id") Long id) {
        return pricingService.findById(id);
    }


    @ApiOperation(value = "Find All")
    @GetMapping
    public List<PricingDto> findAll() {
        return pricingService.findAll();
    }


    @ApiOperation(value = "Update")
    @PutMapping("/{id}")
    public PricingDto update(@RequestBody PricingDto dto,
            @PathVariable("id") Long id) {
        return pricingService.update(id, dto);
    }


    @ApiOperation(value = "Delete by ID")
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        pricingService.deleteById(id);
    }
}
