package by.shag.gritskevich.api.controller;


import java.util.List;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

import by.shag.gritskevich.api.dto.PeriodOfDayPromoDto;
import by.shag.gritskevich.service.PeriodOfDayPromoService;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/periodOfDayPromo")
@RequiredArgsConstructor
public class PeriodOfDayPromoController {

    private final PeriodOfDayPromoService periodOfDayPromoService;


    @ApiOperation(value = "Save")
    @PostMapping("/save")
    public PeriodOfDayPromoDto save(@RequestBody PeriodOfDayPromoDto dto) {
        return periodOfDayPromoService.save(dto);
    }


    @ApiOperation(value = "Find by Id")
    @GetMapping("/{id}")
    public PeriodOfDayPromoDto findById(@PathVariable("id") Long id) {
        return periodOfDayPromoService.findById(id);
    }


    @ApiOperation(value = "Find All")
    @GetMapping
    public List<PeriodOfDayPromoDto> findAll() {
        return periodOfDayPromoService.findAll();
    }


    @ApiOperation(value = "Update")
    @PutMapping("/{id}")
    public PeriodOfDayPromoDto update(@RequestBody PeriodOfDayPromoDto dto,
            @PathVariable("id") Long id) {
        return periodOfDayPromoService.update(id, dto);
    }


    @ApiOperation(value = "Delete by ID")
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        periodOfDayPromoService.deleteById(id);
    }
}
