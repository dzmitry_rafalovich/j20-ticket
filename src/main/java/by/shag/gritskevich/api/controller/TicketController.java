package by.shag.gritskevich.api.controller;


import java.util.List;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

import by.shag.gritskevich.api.dto.TicketDto;
import by.shag.gritskevich.service.TicketService;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/tickets")
@RequiredArgsConstructor
public class TicketController {

    private final TicketService ticketService;


    @ApiOperation(value = "Save")
    @PostMapping("/save")
    public TicketDto save(@RequestBody TicketDto dto) {
        return ticketService.save(dto);
    }


    @ApiOperation(value = "Find by Id")
    @GetMapping("/{id}")
    public TicketDto findById(@PathVariable("id") Long id) {
        return ticketService.findById(id);
    }


    @ApiOperation(value = "Find All")
    @GetMapping
    public List<TicketDto> findAll() {
        return ticketService.findAll();
    }


    @ApiOperation(value = "Update")
    @PutMapping("/{id}")
    public TicketDto update(@RequestBody TicketDto dto,
            @PathVariable("id") Long id) {
        return ticketService.update(id, dto);
    }


    @ApiOperation(value = "Delete by ID")
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        ticketService.deleteById(id);
    }
}
