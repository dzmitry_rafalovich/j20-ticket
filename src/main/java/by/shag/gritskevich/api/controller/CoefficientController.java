package by.shag.gritskevich.api.controller;

import java.util.List;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

import by.shag.gritskevich.api.dto.CoefficientDto;
import by.shag.gritskevich.service.CoefficientService;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/coefficient")
@RequiredArgsConstructor
public class CoefficientController {

    private final CoefficientService coefficientService;


    @ApiOperation(value = "Save")
    @PostMapping("/save")
    public CoefficientDto save(@RequestBody CoefficientDto dto) {
        return coefficientService.save(dto);
    }


    @ApiOperation(value = "Find by Id")
    @GetMapping("/{id}")
    public CoefficientDto findById(@PathVariable("id") Long id) {
        return coefficientService.findById(id);
    }


    @ApiOperation(value = "Find All")
    @GetMapping
    public List<CoefficientDto> findAll() {
        return coefficientService.findAll();
    }


    @ApiOperation(value = "Update")
    @PutMapping("/{id}")
    public CoefficientDto update(@RequestBody CoefficientDto dto,
            @PathVariable("id") Long id) {
        return coefficientService.update(id, dto);
    }


    @ApiOperation(value = "Delete by ID")
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        coefficientService.deleteById(id);
    }
}
