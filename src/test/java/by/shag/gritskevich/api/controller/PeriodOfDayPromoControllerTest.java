package by.shag.gritskevich.api.controller;


import java.time.Instant;
import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import by.shag.gritskevich.api.dto.PeriodOfDayPromoDto;
import by.shag.gritskevich.service.PeriodOfDayPromoService;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = PeriodOfDayPromoController.class)
class PeriodOfDayPromoControllerTest {

    private static final Long ID = 1L;
    private static final Long PRICING_ID = 2L;
    private static final Instant START_TIME = Instant.now();
    private static final Instant END_TIME = Instant.now();
    private static final Integer SAIL = 50;

    @MockBean
    private PeriodOfDayPromoService periodOfDayPromoService;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void save() throws Exception {
        when(periodOfDayPromoService.save(any(PeriodOfDayPromoDto.class))).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.post("/periodOfDayPromo" + "/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.pricingId", is(PRICING_ID)))
                .andExpect(jsonPath("$.startTime", is(START_TIME)))
                .andExpect(jsonPath("$.endTime", is(END_TIME)))
                .andExpect(jsonPath("$.sail", is(SAIL)));
    }


    @Test
    void findById() throws Exception {
        when(periodOfDayPromoService.findById(anyLong())).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.get("/periodOfDayPromo" + "/{id}", 5))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.pricingId", is(PRICING_ID)))
                .andExpect(jsonPath("$.startTime", is(START_TIME)))
                .andExpect(jsonPath("$.endTime", is(END_TIME)))
                .andExpect(jsonPath("$.sail", is(SAIL)));
    }


    @Test
    void findAll() throws Exception {
        when(periodOfDayPromoService.findAll()).thenReturn(Arrays.asList(generateDto(), generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/periodOfDayPromo"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(ID)))
                .andExpect(jsonPath("$.[0].pricingId", is(PRICING_ID)))
                .andExpect(jsonPath("$.[0].startTime", is(START_TIME)))
                .andExpect(jsonPath("$.[0].endTime", is(END_TIME)))
                .andExpect(jsonPath("$.[0].sail", is(SAIL)))
                .andExpect(jsonPath("$.[1].id", is(ID)))
                .andExpect(jsonPath("$.[1].pricingId", is(PRICING_ID)))
                .andExpect(jsonPath("$.[1].startTime", is(START_TIME)))
                .andExpect(jsonPath("$.[1].endTime", is(END_TIME)))
                .andExpect(jsonPath("$.[1].sail", is(SAIL)));
    }


    @Test
    void update() throws Exception {
        when(periodOfDayPromoService.update(anyLong(), any(PeriodOfDayPromoDto.class))).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.put("/periodOfDayPromo" + "/{id}", 5)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto().setId(null))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.pricingId", is(PRICING_ID)))
                .andExpect(jsonPath("$.startTime", is(START_TIME)))
                .andExpect(jsonPath("$.endTime", is(END_TIME)))
                .andExpect(jsonPath("$.sail", is(SAIL)));
    }


    @Test
    void deleteById() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/periodOfDayPromo" + "/{id}", 5))
                .andDo(print())
                .andExpect(status().isOk());
    }


    private PeriodOfDayPromoDto generateDto() {
        return new PeriodOfDayPromoDto()
                .setId(ID)
                .setPricingId(PRICING_ID)
                .setStartHour(START_TIME)
                .setEndHour(END_TIME)
                .setSail(SAIL);
    }
}