package by.shag.gritskevich.api.controller;


import java.time.Instant;
import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import by.shag.gritskevich.api.dto.TicketDto;
import by.shag.gritskevich.service.TicketService;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = TicketController.class)
class TicketControllerTest {

    private static final Long ID = 1L;
    private static final Long SESSION_ID = 2L;
    private static final Long PRICING_ID = 2L;
    private static final Long COEFFICIENT_ID = 2L;
    private static final Long CHAIR_ID = 2L;
    private static final Instant CREATED_AT = Instant.now();
    private static final Integer FINAL_PRICE = 50;
    private static final Long PRICE_CODE = 504032L;

    @MockBean
    private TicketService ticketService;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void save() throws Exception {
        when(ticketService.save(any(TicketDto.class))).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.post("/ticket" + "/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.sessionId", is(SESSION_ID)))
                .andExpect(jsonPath("$.pricingId", is(PRICING_ID)))
                .andExpect(jsonPath("$.coefficientId", is(COEFFICIENT_ID)))
                .andExpect(jsonPath("$.chairId", is(CHAIR_ID)))
                .andExpect(jsonPath("$.createdAt", is(CREATED_AT)))
                .andExpect(jsonPath("$.finalPrice", is(FINAL_PRICE)))
                .andExpect(jsonPath("$.priceCode", is(PRICE_CODE)));
    }


    @Test
    void findById() throws Exception {
        when(ticketService.findById(anyLong())).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.get("/ticket" + "/{id}", 5))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.sessionId", is(SESSION_ID)))
                .andExpect(jsonPath("$.pricingId", is(PRICING_ID)))
                .andExpect(jsonPath("$.coefficientId", is(COEFFICIENT_ID)))
                .andExpect(jsonPath("$.chairId", is(CHAIR_ID)))
                .andExpect(jsonPath("$.createdAt", is(CREATED_AT)))
                .andExpect(jsonPath("$.finalPrice", is(FINAL_PRICE)))
                .andExpect(jsonPath("$.priceCode", is(PRICE_CODE)));
    }


    @Test
    void findAll() throws Exception {
        when(ticketService.findAll()).thenReturn(Arrays.asList(generateDto(), generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/ticket"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(ID)))
                .andExpect(jsonPath("$.[0].sessionId", is(SESSION_ID)))
                .andExpect(jsonPath("$.[0].pricingId", is(PRICING_ID)))
                .andExpect(jsonPath("$.[0].coefficientId", is(COEFFICIENT_ID)))
                .andExpect(jsonPath("$.[0].chairId", is(CHAIR_ID)))
                .andExpect(jsonPath("$.[0].createdAt", is(CREATED_AT)))
                .andExpect(jsonPath("$.[0].finalPrice", is(FINAL_PRICE)))
                .andExpect(jsonPath("$.[0].priceCode", is(PRICE_CODE)))
                .andExpect(jsonPath("$.[1].id", is(ID)))
                .andExpect(jsonPath("$.[1].sessionId", is(SESSION_ID)))
                .andExpect(jsonPath("$.[1].pricingId", is(PRICING_ID)))
                .andExpect(jsonPath("$.[1].coefficientId", is(COEFFICIENT_ID)))
                .andExpect(jsonPath("$.[1].chairId", is(CHAIR_ID)))
                .andExpect(jsonPath("$.[1].createdAt", is(CREATED_AT)))
                .andExpect(jsonPath("$.[1].finalPrice", is(FINAL_PRICE)))
                .andExpect(jsonPath("$.[1].priceCode", is(PRICE_CODE)));
    }


    @Test
    void update() throws Exception {
        when(ticketService.update(anyLong(), any(TicketDto.class))).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.put("/ticket" + "/{id}", 5)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto().setId(null))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.sessionId", is(SESSION_ID)))
                .andExpect(jsonPath("$.pricingId", is(PRICING_ID)))
                .andExpect(jsonPath("$.coefficientId", is(COEFFICIENT_ID)))
                .andExpect(jsonPath("$.chairId", is(CHAIR_ID)))
                .andExpect(jsonPath("$.createdAt", is(CREATED_AT)))
                .andExpect(jsonPath("$.finalPrice", is(FINAL_PRICE)))
                .andExpect(jsonPath("$.priceCode", is(PRICE_CODE)));
    }


    @Test
    void deleteById() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/ticket" + "/{id}", 5))
                .andDo(print())
                .andExpect(status().isOk());
    }


    private TicketDto generateDto() {
        return new TicketDto()
                .setId(ID)
                .setSessionId(SESSION_ID)
                .setPricingId(PRICING_ID)
                .setCoefficientId(COEFFICIENT_ID)
                .setChairId(CHAIR_ID)
                .setCreatedAt(CREATED_AT)
                .setFinalPrice(FINAL_PRICE)
                .setPriceCode(PRICE_CODE);
    }

}