package by.shag.gritskevich.api.controller;


import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import by.shag.gritskevich.api.dto.CoefficientDto;
import by.shag.gritskevich.jpa.model.ChairType;
import by.shag.gritskevich.service.CoefficientService;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = CoefficientController.class)
class CoefficientControllerTest {

    private static final Long ID = 1L;
    private static final ChairType CHAIR_TYPE = ChairType.DEFAULT;
    private static final Integer COEFFICIENT = 5;

    @MockBean
    private CoefficientService coefficientService;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void save() throws Exception {
        when(coefficientService.save(any(CoefficientDto.class))).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.post("/coefficient" + "/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.chairType", is(CHAIR_TYPE)))
                .andExpect(jsonPath("$.coefficient", is(COEFFICIENT)));
    }


    @Test
    void findById() throws Exception {
        when(coefficientService.findById(anyLong())).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.get("/coefficient" + "/{id}", 5))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.chairType", is(CHAIR_TYPE)))
                .andExpect(jsonPath("$.coefficient", is(COEFFICIENT)));
    }


    @Test
    void findAll() throws Exception {
        when(coefficientService.findAll()).thenReturn(Arrays.asList(generateDto(), generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/coefficient"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(ID)))
                .andExpect(jsonPath("$.[0].chairType", is(CHAIR_TYPE)))
                .andExpect(jsonPath("$.[0].coefficient", is(COEFFICIENT)))
                .andExpect(jsonPath("$.[1].id", is(ID)))
                .andExpect(jsonPath("$.[1].chairType", is(CHAIR_TYPE)))
                .andExpect(jsonPath("$.[1].coefficient", is(COEFFICIENT)));
    }


    @Test
    void update() throws Exception {
        when(coefficientService.update(anyLong(), any(CoefficientDto.class))).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.put("/coefficient" + "/{id}", 5)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto().setId(null))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.chairType", is(CHAIR_TYPE)))
                .andExpect(jsonPath("$.coefficient", is(COEFFICIENT)));
    }


    @Test
    void deleteById() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/coefficient" + "/{id}", 5))
                .andDo(print())
                .andExpect(status().isOk());
    }


    private CoefficientDto generateDto() {
        return new CoefficientDto()
                .setId(ID)
                .setChairType(CHAIR_TYPE)
                .setCoefficient(COEFFICIENT);
    }

}