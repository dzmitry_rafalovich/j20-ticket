package by.shag.gritskevich.api.controller;


import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import by.shag.gritskevich.api.dto.PricingDto;
import by.shag.gritskevich.service.PricingService;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = PricingController.class)
class PricingControllerTest {

    private static final Long ID = 1L;
    private static final Long FILM_ID = 2L;
    private static final Long CINEMA_ID = 3L;
    private static final Long HALL_ID = 4L;
    private static final Integer DEFAULT_PRICE = 5;

    @MockBean
    private PricingService pricingService;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void save() throws Exception {
        when(pricingService.save(any(PricingDto.class))).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.post("/pricing" + "/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.filmId", is(FILM_ID)))
                .andExpect(jsonPath("$.cinemaId", is(CINEMA_ID)))
                .andExpect(jsonPath("$.hallId", is(HALL_ID)))
                .andExpect(jsonPath("$.defaultPrice", is(DEFAULT_PRICE)));
    }


    @Test
    void findById() throws Exception {
        when(pricingService.findById(anyLong())).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.get("/pricing" + "/{id}", 5))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.filmId", is(FILM_ID)))
                .andExpect(jsonPath("$.cinemaId", is(CINEMA_ID)))
                .andExpect(jsonPath("$.hallId", is(HALL_ID)))
                .andExpect(jsonPath("$.defaultPrice", is(DEFAULT_PRICE)));
    }


    @Test
    void findAll() throws Exception {
        when(pricingService.findAll()).thenReturn(Arrays.asList(generateDto(), generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/pricing"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(ID)))
                .andExpect(jsonPath("$.[0].filmId", is(FILM_ID)))
                .andExpect(jsonPath("$.[0].cinemaId", is(CINEMA_ID)))
                .andExpect(jsonPath("$.[0].hallId", is(HALL_ID)))
                .andExpect(jsonPath("$.[0].defaultPrice", is(DEFAULT_PRICE)))
                .andExpect(jsonPath("$.[1].id", is(ID)))
                .andExpect(jsonPath("$.[1].filmId", is(FILM_ID)))
                .andExpect(jsonPath("$.[1].cinemaId", is(CINEMA_ID)))
                .andExpect(jsonPath("$.[1].hallId", is(HALL_ID)))
                .andExpect(jsonPath("$.[1].defaultPrice", is(DEFAULT_PRICE)));
    }


    @Test
    void update() throws Exception {
        when(pricingService.update(anyLong(), any(PricingDto.class))).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.put("/pricing" + "/{id}", 5)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto().setId(null))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.filmId", is(FILM_ID)))
                .andExpect(jsonPath("$.cinemaId", is(CINEMA_ID)))
                .andExpect(jsonPath("$.hallId", is(HALL_ID)))
                .andExpect(jsonPath("$.defaultPrice", is(DEFAULT_PRICE)));
    }


    @Test
    void deleteById() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/pricing" + "/{id}", 5))
                .andDo(print())
                .andExpect(status().isOk());
    }


    private PricingDto generateDto() {
        return new PricingDto()
                .setId(ID)
                .setFilmId(FILM_ID)
                .setCinemaId(CINEMA_ID)
                .setHallId(HALL_ID)
                .setDefaultPrice(DEFAULT_PRICE);
    }
}