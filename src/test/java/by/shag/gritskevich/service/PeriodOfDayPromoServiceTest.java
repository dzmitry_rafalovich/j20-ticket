package by.shag.gritskevich.service;


import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import by.shag.gritskevich.api.dto.PeriodOfDayPromoDto;
import by.shag.gritskevich.exceptions.EntityNotFoundException;
import by.shag.gritskevich.jpa.model.PeriodOfDayPromo;
import by.shag.gritskevich.jpa.repository.PeriodOfDayPromoRepository;
import by.shag.gritskevich.mapper.PeriodOfDayPromoMapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class PeriodOfDayPromoServiceTest {

    private static final Long ID = 12L;

    @Mock
    private PeriodOfDayPromoRepository periodOfDayPromoRepository;

    @Mock
    private PeriodOfDayPromoMapper periodOfDayPromoMapper;

    @InjectMocks
    private PeriodOfDayPromoService periodOfDayPromoService;


    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(periodOfDayPromoRepository, periodOfDayPromoMapper);
    }


    @Test
    void save() {
        var periodOfDayPromo = mock(PeriodOfDayPromo.class);
        when(periodOfDayPromoMapper.mapFromDto(any(PeriodOfDayPromoDto.class))).thenReturn(periodOfDayPromo);
        var periodOfDayPromoAfterSaving = mock(PeriodOfDayPromo.class);
        when(periodOfDayPromoRepository.save(any(PeriodOfDayPromo.class))).thenReturn(periodOfDayPromoAfterSaving);
        var periodOfDayPromoDtoReturn = mock(PeriodOfDayPromoDto.class);
        when(periodOfDayPromoMapper.mapFromModel(any(PeriodOfDayPromo.class))).thenReturn(periodOfDayPromoDtoReturn);

        var periodOfDayPromoDto = mock(PeriodOfDayPromoDto.class);
        var result = periodOfDayPromoService.save(periodOfDayPromoDto);

        assertEquals(result, periodOfDayPromoDtoReturn);

        verify(periodOfDayPromoMapper).mapFromDto(periodOfDayPromoDto);
        verify(periodOfDayPromoRepository).save(periodOfDayPromo);
        verify(periodOfDayPromoMapper).mapFromModel(periodOfDayPromoAfterSaving);
    }


    @Test
    void findById() {
        var periodOfDayPromo = mock(PeriodOfDayPromo.class);
        when(periodOfDayPromoRepository.findById(ID)).thenReturn(Optional.of(periodOfDayPromo));
        var periodOfDayPromoDto = mock(PeriodOfDayPromoDto.class);
        when(periodOfDayPromoMapper.mapFromModel(any(PeriodOfDayPromo.class))).thenReturn(periodOfDayPromoDto);

        var result = periodOfDayPromoService.findById(ID);

        assertEquals(result, periodOfDayPromoDto);

        verify(periodOfDayPromoRepository).findById(ID);
        verify(periodOfDayPromoMapper).mapFromModel(periodOfDayPromo);
    }


    @Test
    void findByIdWhenNoSuchPeriodOfDayPromo() {
        when(periodOfDayPromoRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,
                () -> periodOfDayPromoService.findById(ID));

        verify(periodOfDayPromoRepository).findById(ID);
    }


    @Test
    void findAll() {
        var model = mock(PeriodOfDayPromo.class);
        var modelList = Arrays.asList(model, model, model);
        when(periodOfDayPromoRepository.findAll()).thenReturn(modelList);
        var mapped = mock(PeriodOfDayPromoDto.class);
        var mappedList = Arrays.asList(mapped, mapped, mapped);
        when(periodOfDayPromoMapper.mapListFromModel(anyList())).thenReturn(mappedList);

        var result = periodOfDayPromoService.findAll();

        assertArrayEquals(result.toArray(), mappedList.toArray());

        verify(periodOfDayPromoRepository).findAll();
        verify(periodOfDayPromoMapper).mapListFromModel(modelList);
    }


    @Test
    void update() {
        var periodOfDayPromo = mock(PeriodOfDayPromo.class);
        when(periodOfDayPromoMapper.mapFromDto(any(PeriodOfDayPromoDto.class))).thenReturn(periodOfDayPromo);
        var periodOfDayPromoAfterSaving = mock(PeriodOfDayPromo.class);
        when(periodOfDayPromoRepository.save(any(PeriodOfDayPromo.class))).thenReturn(periodOfDayPromoAfterSaving);
        var periodOfDayPromoDtoReturned = mock(PeriodOfDayPromoDto.class);
        when(periodOfDayPromoMapper.mapFromModel(any(PeriodOfDayPromo.class))).thenReturn(periodOfDayPromoDtoReturned);

        var periodOfDayPromoDto = mock(PeriodOfDayPromoDto.class);
        var result = periodOfDayPromoService.update(ID, periodOfDayPromoDto);

        assertEquals(result, periodOfDayPromoDtoReturned);

        verify(periodOfDayPromoMapper).mapFromDto(periodOfDayPromoDto);
        verify(periodOfDayPromoRepository).save(periodOfDayPromo);
        verify(periodOfDayPromoMapper).mapFromModel(periodOfDayPromoAfterSaving);
    }


    @Test
    void deleteById() {
        periodOfDayPromoService.deleteById(ID);

        verify(periodOfDayPromoRepository).deleteById(ID);
    }
}