package by.shag.gritskevich.service;


import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import by.shag.gritskevich.api.dto.TicketDto;
import by.shag.gritskevich.exceptions.EntityNotFoundException;
import by.shag.gritskevich.jpa.model.Ticket;
import by.shag.gritskevich.jpa.repository.TicketRepository;
import by.shag.gritskevich.mapper.TicketMapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class TicketServiceTest {

    private static final Long ID = 12L;

    @Mock
    private TicketRepository ticketRepository;

    @Mock
    private TicketMapper ticketMapper;

    @InjectMocks
    private TicketService ticketService;


    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(ticketRepository, ticketMapper);
    }


    @Test
    void save() {
        var ticket = mock(Ticket.class);
        when(ticketMapper.mapFromDto(any(TicketDto.class))).thenReturn(ticket);
        var ticketAfterSaving = mock(Ticket.class);
        when(ticketRepository.save(any(Ticket.class))).thenReturn(ticketAfterSaving);
        var ticketDtoReturn = mock(TicketDto.class);
        when(ticketMapper.mapFromModel(any(Ticket.class))).thenReturn(ticketDtoReturn);

        var ticketDto = mock(TicketDto.class);
        var result = ticketService.save(ticketDto);

        assertEquals(result, ticketDtoReturn);

        verify(ticketMapper).mapFromDto(ticketDto);
        verify(ticketRepository).save(ticket);
        verify(ticketMapper).mapFromModel(ticketAfterSaving);
    }


    @Test
    void findById() {
        var ticket = mock(Ticket.class);
        when(ticketRepository.findById(ID)).thenReturn(Optional.of(ticket));
        var ticketDto = mock(TicketDto.class);
        when(ticketMapper.mapFromModel(any(Ticket.class))).thenReturn(ticketDto);

        var result = ticketService.findById(ID);

        assertEquals(result, ticketDto);

        verify(ticketRepository).findById(ID);
        verify(ticketMapper).mapFromModel(ticket);
    }


    @Test
    void findByIdWhenNoSuchTicket() {
        when(ticketRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,
                () -> ticketService.findById(ID));

        verify(ticketRepository).findById(ID);
    }


    @Test
    void findAll() {
        var model = mock(Ticket.class);
        var modelList = Arrays.asList(model, model, model);
        when(ticketRepository.findAll()).thenReturn(modelList);
        var mapped = mock(TicketDto.class);
        var mappedList = Arrays.asList(mapped, mapped, mapped);
        when(ticketMapper.mapListFromModel(anyList())).thenReturn(mappedList);

        var result = ticketService.findAll();

        assertArrayEquals(result.toArray(), mappedList.toArray());

        verify(ticketRepository).findAll();
        verify(ticketMapper).mapListFromModel(modelList);
    }


    @Test
    void update() {
        var ticket = mock(Ticket.class);
        when(ticketMapper.mapFromDto(any(TicketDto.class))).thenReturn(ticket);
        var ticketAfterSaving = mock(Ticket.class);
        when(ticketRepository.save(any(Ticket.class))).thenReturn(ticketAfterSaving);
        var ticketDtoReturned = mock(TicketDto.class);
        when(ticketMapper.mapFromModel(any(Ticket.class))).thenReturn(ticketDtoReturned);

        var ticketDto = mock(TicketDto.class);
        var result = ticketService.update(ID, ticketDto);

        assertEquals(result, ticketDtoReturned);

        verify(ticketMapper).mapFromDto(ticketDto);
        verify(ticketRepository).save(ticket);
        verify(ticketMapper).mapFromModel(ticketAfterSaving);
    }


    @Test
    void deleteById() {
        ticketService.deleteById(ID);

        verify(ticketRepository).deleteById(ID);
    }
}