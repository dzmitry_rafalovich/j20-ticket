package by.shag.gritskevich.service;

import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import by.shag.gritskevich.api.dto.PricingDto;
import by.shag.gritskevich.exceptions.EntityNotFoundException;
import by.shag.gritskevich.jpa.model.Pricing;
import by.shag.gritskevich.jpa.repository.PricingRepository;
import by.shag.gritskevich.mapper.PricingMapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class PricingServiceTest {

    private static final Long ID = 12L;

    @Mock
    private PricingRepository pricingRepository;

    @Mock
    private PricingMapper pricingMapper;

    @InjectMocks
    private PricingService pricingService;


    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(pricingRepository, pricingMapper);
    }


    @Test
    void save() {
        var pricing = mock(Pricing.class);
        when(pricingMapper.mapFromDto(any(PricingDto.class))).thenReturn(pricing);
        var pricingAfterSaving = mock(Pricing.class);
        when(pricingRepository.save(any(Pricing.class))).thenReturn(pricingAfterSaving);
        var pricingDtoReturn = mock(PricingDto.class);
        when(pricingMapper.mapFromModel(any(Pricing.class))).thenReturn(pricingDtoReturn);

        var pricingDto = mock(PricingDto.class);
        var result = pricingService.save(pricingDto);

        assertEquals(result, pricingDtoReturn);

        verify(pricingMapper).mapFromDto(pricingDto);
        verify(pricingRepository).save(pricing);
        verify(pricingMapper).mapFromModel(pricingAfterSaving);
    }


    @Test
    void findById() {
        var pricing = mock(Pricing.class);
        when(pricingRepository.findById(ID)).thenReturn(Optional.of(pricing));
        var pricingDto = mock(PricingDto.class);
        when(pricingMapper.mapFromModel(any(Pricing.class))).thenReturn(pricingDto);

        var result = pricingService.findById(ID);

        assertEquals(result, pricingDto);

        verify(pricingRepository).findById(ID);
        verify(pricingMapper).mapFromModel(pricing);
    }


    @Test
    void findByIdWhenNoSuchPricing() {
        when(pricingRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,
                () -> pricingService.findById(ID));

        verify(pricingRepository).findById(ID);
    }


    @Test
    void findAll() {
        var model = mock(Pricing.class);
        var modelList = Arrays.asList(model, model, model);
        when(pricingRepository.findAll()).thenReturn(modelList);
        var mapped = mock(PricingDto.class);
        var mappedList = Arrays.asList(mapped, mapped, mapped);
        when(pricingMapper.mapListFromModel(anyList())).thenReturn(mappedList);

        var result = pricingService.findAll();

        assertArrayEquals(result.toArray(), mappedList.toArray());

        verify(pricingRepository).findAll();
        verify(pricingMapper).mapListFromModel(modelList);
    }


    @Test
    void update() {
        var pricing = mock(Pricing.class);
        when(pricingMapper.mapFromDto(any(PricingDto.class))).thenReturn(pricing);
        var pricingAfterSaving = mock(Pricing.class);
        when(pricingRepository.save(any(Pricing.class))).thenReturn(pricingAfterSaving);
        var pricingDtoReturned = mock(PricingDto.class);
        when(pricingMapper.mapFromModel(any(Pricing.class))).thenReturn(pricingDtoReturned);

        var pricingDto = mock(PricingDto.class);
        var result = pricingService.update(ID, pricingDto);

        assertEquals(result, pricingDtoReturned);

        verify(pricingMapper).mapFromDto(pricingDto);
        verify(pricingRepository).save(pricing);
        verify(pricingMapper).mapFromModel(pricingAfterSaving);
    }


    @Test
    void deleteById() {
        pricingService.deleteById(ID);

        verify(pricingRepository).deleteById(ID);
    }
}