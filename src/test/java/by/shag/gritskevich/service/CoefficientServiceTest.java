package by.shag.gritskevich.service;


import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import by.shag.gritskevich.api.dto.CoefficientDto;
import by.shag.gritskevich.exceptions.EntityNotFoundException;
import by.shag.gritskevich.jpa.model.Coefficient;
import by.shag.gritskevich.jpa.repository.CoefficientRepository;
import by.shag.gritskevich.mapper.CoefficientMapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class CoefficientServiceTest {

    private static final Long ID = 12L;
    
    @Mock
    private CoefficientRepository coefficientRepository;
    
    @Mock
    private CoefficientMapper coefficientMapper;
    
    @InjectMocks
    private CoefficientService coefficientService;


    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(coefficientRepository, coefficientMapper);
    }


    @Test
    void save() {
        var coefficient = mock(Coefficient.class);
        when(coefficientMapper.mapFromDto(any(CoefficientDto.class))).thenReturn(coefficient);
        var coefficientAfterSaving = mock(Coefficient.class);
        when(coefficientRepository.save(any(Coefficient.class))).thenReturn(coefficientAfterSaving);
        var coefficientDtoReturn = mock(CoefficientDto.class);
        when(coefficientMapper.mapFromModel(any(Coefficient.class))).thenReturn(coefficientDtoReturn);

        var coefficientDto = mock(CoefficientDto.class);
        var result = coefficientService.save(coefficientDto);

        assertEquals(result, coefficientDtoReturn);

        verify(coefficientMapper).mapFromDto(coefficientDto);
        verify(coefficientRepository).save(coefficient);
        verify(coefficientMapper).mapFromModel(coefficientAfterSaving);
    }


    @Test
    void findById() {
        var coefficient = mock(Coefficient.class);
        when(coefficientRepository.findById(ID)).thenReturn(Optional.of(coefficient));
        var coefficientDto = mock(CoefficientDto.class);
        when(coefficientMapper.mapFromModel(any(Coefficient.class))).thenReturn(coefficientDto);

        var result = coefficientService.findById(ID);

        assertEquals(result, coefficientDto);

        verify(coefficientRepository).findById(ID);
        verify(coefficientMapper).mapFromModel(coefficient);
    }


    @Test
    void findByIdWhenNoSuchCoefficient() {
        when(coefficientRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,
                () -> coefficientService.findById(ID));

        verify(coefficientRepository).findById(ID);
    }


    @Test
    void findAll() {
        var model = mock(Coefficient.class);
        var modelList = Arrays.asList(model, model, model);
        when(coefficientRepository.findAll()).thenReturn(modelList);
        var mapped = mock(CoefficientDto.class);
        var mappedList = Arrays.asList(mapped, mapped, mapped);
        when(coefficientMapper.mapListFromModel(anyList())).thenReturn(mappedList);

        var result = coefficientService.findAll();

        assertArrayEquals(result.toArray(), mappedList.toArray());

        verify(coefficientRepository).findAll();
        verify(coefficientMapper).mapListFromModel(modelList);
    }


    @Test
    void update() {
        var coefficient = mock(Coefficient.class);
        when(coefficientMapper.mapFromDto(any(CoefficientDto.class))).thenReturn(coefficient);
        var coefficientAfterSaving = mock(Coefficient.class);
        when(coefficientRepository.save(any(Coefficient.class))).thenReturn(coefficientAfterSaving);
        var coefficientDtoReturned = mock(CoefficientDto.class);
        when(coefficientMapper.mapFromModel(any(Coefficient.class))).thenReturn(coefficientDtoReturned);

        var coefficientDto = mock(CoefficientDto.class);
        var result = coefficientService.update(ID, coefficientDto);

        assertEquals(result, coefficientDtoReturned);

        verify(coefficientMapper).mapFromDto(coefficientDto);
        verify(coefficientRepository).save(coefficient);
        verify(coefficientMapper).mapFromModel(coefficientAfterSaving);
    }


    @Test
    void deleteById() {
        coefficientService.deleteById(ID);

        verify(coefficientRepository).deleteById(ID);
    }
}